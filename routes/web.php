<?php
/** @var $router \Laravel\Lumen\Routing\Router */

$router->get('/', 'AppController@home');

$router->get('/login', 'AppController@login');
$router->get('/register', 'AppController@register');

$router->get('/user/profile', 'AppController@userProfile');
$router->get('/products/create', 'AppController@createProduct');
$router->post('/add-to-basket', 'AppController@addProductToBasket');
$router->get('/voir-panier', 'AppController@viewBasket');
$router->get('/accueil', 'AppController@viewAllProducts');
$router->get('/test','AppController@test');
$router->post('/test','AppController@createProduct');

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('/users', 'AppApiController@registerUser');
    $router->post('/users/login', 'AppApiController@loginUser');
    $router->post('/code/check', 'AppApiController@checkCode');
});