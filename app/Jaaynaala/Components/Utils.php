<?php

namespace App\Jaaynaala\Components;


class Utils {

    /*public static function sendSMS($receiver, $message) {
        $sid = "ACb6544a3f1c50b631ca81d5a225d201bb";
        $token = "325e3fa1f01baf5c3ada8afbc34548ea";
        try {
            $twilio = new \Twilio\Rest\Client($sid, $token);
        } catch (\Twilio\Exceptions\ConfigurationException $e) {
            return false;
        }

        $message = $twilio->messages
            ->create("+221778140352", // to
                array(
                    "body" => "$message",
                    "from" => "+18318880640"
                )
            );
        return true;
    }*/

    /**
     * @param $extension
     * @return bool
     */
    public static function isExtensionValid($extension){//Pour verifier l'extension d'une photo
        $extensionValid = array('png','PNG','jpg','JPG','jpeg','JPEG');
        $isIn = false;
        foreach ($extensionValid as $value){
            if($extension === $value){
                $isIn = true;
            }
        }
        return $isIn;
    }

    /**
     * @return string
     */
    public static function getAnameForFile(){
        //Pour generer une chaine de caracteres aleatoire
        $file_name = [];
        $str = str_split('0123456789abcddefghijklmnopqrstuvwxyzABCCDEFGHIJKLMNOPQRSTUVWXYZ');
        $length = rand(10,62);
        for($i=0;$i<$length;$i++){
            $file_name[] = $str[rand(0,61)];
        }
        return implode('',$file_name).'.png';
    }

    public static function sendSMS($phone, $sms) {
        $sid = "AC132f334d2d55c853b438bd16d40c596e";
        $token = "95efc18346cd848e74781d3af9ab4c75";
        try {
            $twilio = new \Twilio\Rest\Client($sid, $token);
        } catch (\Twilio\Exceptions\ConfigurationException $e) {
            return false;
        }

        $message = $twilio->messages
            ->create("+221$phone", // to
                array(
                    "body" => "$sms",
                    "from" => "Jaaynaala"
                )
            );
        return true;
    }

    public static function sendConfirmationCodeSMS($phoneNumber, $code) {
        return self::sendSMS($phoneNumber, "Bienvenue dans le réseau de Jaaynaala. Pour terminer votre inscription voici votre code de confirmation: $code");
    }

    public static function simulateSendConfirmationCode($code) {
        return true;
    }

    public static function generateConfirmationCode() {
        return rand(1000, 10000- 1);
    }
}
