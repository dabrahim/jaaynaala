<?php

namespace App\Jaaynaala\Components\Model;


class VerificationCode {
    private $_id;
    private $_code;
    private $_expirationCode;
    private $_type;
    private $_userId;

    const TYPE_PHONE_VERIFICATION = "PHONE_VERIFICATION";

    /**
     * VerificationCode constructor.
     * @param $_code
     * @param $_type
     * @param $_userId
     */
    public function __construct($_code, $_type, $_userId)
    {
        $this->_code = $_code;
        $this->_type = $_type;
        $this->_userId = $_userId;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->_code = $code;
    }

    /**
     * @return mixed
     */
    public function getExpirationCode()
    {
        return $this->_expirationCode;
    }

    /**
     * @param mixed $expirationCode
     */
    public function setExpirationCode($expirationCode): void
    {
        $this->_expirationCode = $expirationCode;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->_type = $type;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->_userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->_userId = $userId;
    }

}