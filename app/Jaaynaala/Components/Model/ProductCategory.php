<?php
    namespace App\Jaaynaala\Components\Model;

    class ProductCategory{
        private $_id;
        private $_name;

        /**
         * ProductCategory constructor.
         * @param $_name
         */
        public function __construct($_name)
        {
            $this->_name = $_name;
        }


        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->_id;
        }

        /**
         * @param mixed $id
         */
        public function setId($id): void
        {
            $this->_id = $id;
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->_name;
        }

        /**
         * @param mixed $name
         */
        public function setName($name): void
        {
            $this->_name = $name;
        }


    }
