<?php

namespace App\Jaaynaala\Components\Model;


class ApiResponseManager {
    public static function getSuccessfulResponse($message = null, $data = null) {
        $result = array(
            'success' => true,
        );

        if ($message !== null) {
            $response = array(
                'message' => $message
            );

            if ($data !== null) {
                $response['data'] = $data;
            }

            $result['response'] = $response;
        } else {
            $result = $data;
        }

        return response()->json($result);
    }

    public static function getErrorResponse($message) {
    }

    public static function getUnSuccessfulResponse($message) {
        $result = array(
            'success' => false,
            'error' => array(
                'message' => $message
            )
        );
        return response()->json($result);
    }
}