<?php
/**
 * Created by PhpStorm.
 * User: dabrahim
 * Date: 1/18/19
 * Time: 12:58 PM
 */

namespace App\Jaaynaala\Components\Model {

//DTO Data Transfer Object
    class Product
    {
        private $_id;
        private $_name;
        private $_pic_url;
        private $_description;
        private $_publicationDate;
        private $_price;
        private $_productCategoryId;
        private $_sellerId;

        /**
         * Product constructor.
         * @param $_id
         * @param $_name
         * @param $_description
         * @param $_publicationDate
         * @param $_price
         * @param $_productCategoryId
         * @param $_sellerId
         */
        public function __construct($_name,$_pic_url, $_description, $_price, $_productCategoryId, $_sellerId)
        {
            $this->setName($_name);
            $this->setPicUrl($_pic_url);
            $this->setDescription($_description);
            $this->setPrice($_price);
            $this->setProductCategoryId($_productCategoryId);
            $this->setSellerId($_sellerId);
        }

        /**
         * @return mixed
         */
        public function getPicUrl()
        {
            return $this->_pic_url;
        }

        /**
         * @param mixed $pic_url
         */
        public function setPicUrl($pic_url): void
        {
            $this->_pic_url = $pic_url;
        }

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->_id;
        }

        /**
         * @param mixed $id
         */
        public function setId($id): void
        {
            $this->_id = $id;
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->_name;
        }

        /**
         * @param mixed $name
         */
        public function setName($name): void
        {
            $this->_name = $name;
        }

        /**
         * @return mixed
         */
        public function getDescription()
        {
            return $this->_description;
        }

        /**
         * @param mixed $description
         */
        public function setDescription($description): void
        {
            $this->_description = $description;
        }

        /**
         * @return mixed
         */
        public function getPublicationDate()
        {
            return $this->_publicationDate;
        }

        /**
         * @param mixed $publicationDate
         */
        public function setPublicationDate($publicationDate): void
        {
            $this->_publicationDate = $publicationDate;
        }

        /**
         * @return mixed
         */
        public function getPrice()
        {
            return $this->_price;
        }

        /**
         * @param mixed $price
         */
        public function setPrice($price): void
        {
            $this->_price = $price;
        }

        /**
         * @return mixed
         */
        public function getProductCategoryId()
        {
            return $this->_productCategoryId;
        }

        /**
         * @param mixed $productCategoryId
         */
        public function setProductCategoryId($productCategoryId): void
        {
            $this->_productCategoryId = $productCategoryId;
        }

        /**
         * @return mixed
         */
        public function getSellerId()
        {
            return $this->_sellerId;
        }

        /**
         * @param mixed $sellerId
         */
        public function setSellerId($sellerId): void
        {
            $this->_sellerId = $sellerId;
        }
    }
}
