<?php

namespace App\Jaaynaala\Components\Service;
use App\Jaaynaala\Components\Model\Product;


    class ProductService extends AbstractService {

        /**
         * @param Product $product
         * @return int
         */
        public function create(Product $product) {//Permet d'inserer un produit sur la base de donnees
            return $this->_pdo->exec("INSERT INTO product SET
              product_name= '{$product->getName()}',
              pic_url = '{$product->getPicUrl()}',
              description='{$product->getDescription()}',
              publicationDate=NOW(),
              price='{$product->getPrice()}',
              id_product_category='{$product->getProductCategoryId()}',
              id_seller='{$product->getSellerId()}'");
        }

        /**
         * @param $sellerId
         * @return array
         */
        public function findBySellerId($sellerId) {//Permet de trouver les produits inserer par un utilisateur
            $_pdoStatement = $this->_pdo->query("SELECT * FROM product WHERE seller_id = $sellerId ORDER BY id DESC");
            $_pdoStatement->execute();
            return $_pdoStatement->fetchAll(\PDO::FETCH_ASSOC);
        }

    }
