<?php

namespace App\Jaaynaala\Components\Service;

use PDO;
use App\Jaaynaala\Components\Model\VerificationCode;

class VerificationCodeService extends AbstractService {

    public function create(VerificationCode $code) {
        $pdoStatement = $this->_pdo->prepare("INSERT INTO verification_code SET code = :code, expiration_date = DATE_ADD(now(), INTERVAL 1 HOUR), user_id = :userId, type = :codeType");
        return $pdoStatement->execute(array(
            'code' => $code->getCode(),
            'userId' => $code->getUserId(),
            'codeType' => $code->getType()
        ));
    }

    public function isValid($code) {
        $pdoStatement = $this->_pdo->prepare("SELECT 1 FROM verification_code WHERE code = :code AND expiration_date > NOW()");
        $pdoStatement->execute(array(
            ':code' => $code
        ));
        return $pdoStatement->fetch() !== false;
    }

    public function getLatestValidCodeByUserId($userId) {
        $pdoStatement = $this->_pdo->prepare("SELECT code FROM verification_code WHERE user_id = :userId AND expiration_date > NOW() ORDER BY id DESC LIMIT 1");
        $pdoStatement->execute(array(
            'userId' => $userId
        ));
        if ($code = $pdoStatement->fetch(PDO::FETCH_COLUMN)) {
            return $code;
        }
        return -1;
    }

    public function deleteByUserId($userId) {
        $pdoStatement = $this->_pdo->prepare("DELETE FROM verification_code WHERE user_id = :userId");
        return $pdoStatement->execute(array(
            'userId' => $userId
        ));
    }
}