<?php
/**
 * Created by PhpStorm.
 * User: dabrahim
 * Date: 1/18/19
 * Time: 1:07 PM
 */

namespace App\Jaaynaala\Components\Service;

use PDO;

abstract class AbstractService {
    /**
     * @var PDO
     */
    protected $_pdo;

    public function __construct() {
        $this->_pdo = new PDO($_ENV['DB_CONNECTION'] . ":host=" . $_ENV['DB_HOST'] . ";dbname=" . $_ENV['DB_DATABASE'], $_ENV['DB_USERNAME'], $_ENV['DB_PASSWORD']);
        $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}