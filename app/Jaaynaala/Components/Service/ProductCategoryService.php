<?php

namespace App\Components\Service;

use App\Jaaynaala\Components\Model\ProductCategory;
use App\Jaaynaala\Components\Service\AbstractService;
use PDO;

class ProductCategoryService extends AbstractService {
    public function createCategory(ProductCategory $category) {
        $query = $this->_pdo->prepare("INSERT INTO product_category (product_category_name) VALUES (:pcName)");
        return $query->execute(array(
            'pcName' => $category->getName()
        ));
    }

    public function getAll() {
        $query = $this->_pdo->query('SELECT * FROM product_category') or die(print_r($this->_pdo->errorInfo()));
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}
