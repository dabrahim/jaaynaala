<?php

namespace App\Jaaynaala\Components\Service;

use App\Jaaynaala\Components\Model\User;

class UserService extends AbstractService {

    public function register(User $user) {
		$pdoStatement = $this->_pdo->prepare("INSERT INTO user SET first_name = :firstName, last_name = :lastName, pseudo = :pseudo, password = :password, registration_date = NOW(), type = :userType, phone_number=:phoneNumber");
		$pdoStatement->bindValue(":firstName", $user->getFirstName());
		$pdoStatement->bindValue(":lastName", $user->getLastName());
		$pdoStatement->bindValue(":pseudo", $user->getLogin());
		$pdoStatement->bindValue(":password", $user->getPassword());
		$pdoStatement->bindValue(":userType", $user->getType());
		$pdoStatement->bindValue(":phoneNumber", $user->getPhoneNumber());

		return $pdoStatement->execute() ? $this->_pdo->lastInsertId() : -1;
	}

	public function pseudoExists($pseudo) {
		$pdoStatement = $this->_pdo->prepare("SELECT 1 FROM user  WHERE pseudo = :pseudo");
		$pdoStatement->bindValue(":pseudo", $pseudo, \PDO::PARAM_STR);
		$pdoStatement->execute();
		return $pdoStatement->fetch() !== false;
	}

	public function isVerified($userId) {

    }

	public function findByPseudo($pseudo) {
		$pdoStatement = $this->_pdo->prepare("SELECT * FROM user WHERE pseudo =:pseudo");
		$pdoStatement->bindValue(":pseudo", $pseudo, \PDO::PARAM_STR);
		$pdoStatement->execute();
		return $pdoStatement->fetch(\PDO::FETCH_ASSOC);
	}

    public function findByLogin($login) {
        $pdoStatement = $this->_pdo->prepare("SELECT * FROM user WHERE pseudo =:login OR phone_number = :login");
        $pdoStatement->bindValue(":login", $login);
        $pdoStatement->execute();
        return $pdoStatement->fetch(\PDO::FETCH_ASSOC);
    }

	public function isValidAccount($login, $password) {
		$pdoStatement = $this->_pdo->query("SELECT 1 FROM user WHERE (pseudo = '$login' OR phone_number = '$login') and password = '$password'");
		$pdoStatement->execute();
		return $pdoStatement->fetch() !== false;
	}
}
