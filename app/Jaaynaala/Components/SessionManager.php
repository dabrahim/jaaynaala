<?php
/**
 * Created by PhpStorm.
 * User: diadji
 * Date: 1/24/19
 * Time: 11:51 PM
 */

namespace App\Jaaynaala\Components;


class SessionManager
{
    private static $instance;


    public function __construct()
    {
        session_start();
    }



    public function isLogin(){
        $response = false;
        if(isset($_SESSION['id']) && isset($_SESSION['pseudo']))
            $response = true;

        return $response;
    }

    public static function getInstance(){
        if(self::$instance == null)
            self::$instance = new SessionManager();

        return  self::$instance;
    }
}
