<?php

namespace App\Http\Controllers;

use App\Jaaynaala\Components\Model\User;
use App\Jaaynaala\Components\Model\Product;
use App\Jaaynaala\Components\Service\ProductService;
use App\Jaaynaala\Components\Service\UserService;
use App\Jaaynaala\Components\SessionManager;
use App\Jaaynaala\Components\Utils;
use Illuminate\Http\Request;

class AppController extends Controller {

    function home() {
        return view('home');
    }

    public function login(Request $request) {
        return view('login');
    }

    public function register(Request $request) {
        return view('register');
    }

    public function userProfile() {
    }

    public function createProduct(Request $request) {
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            if (Utils::isExtensionValid($file->getClientOriginalExtension())) {
                $name = $request->input('name');
                $file_name = Utils::getAnameForFile();
                $pic_url = 'photo/' . $file_name;
                $description = $request->input('description');
                $price = $request->input('price');
                $categoryId = $request->input('category_id');
                $sellerId = $request->input('seller_id');

                $product = new Product($name, $pic_url, $description, $price, $categoryId, $sellerId);

                $productService = new ProductService();

                try {
                    $productService->create($product);
                    $file->move('photo/', $file_name);
                    return "ok";
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            } else {
                return 'L\'extension n\'est pas valide(Extension valide : jpg,JPG,jpeg,JPEG,png,PNG)';
            }
        } else {
            return 'Veuillez mettre une photo sur le produit';
        }
    }


    public function addProductToBasket(Request $request) {
        $productId = $request->get('product-id');
        $sessionManager = SessionManager::getInstance();
        if ($sessionManager->isLogin()) {
            if (!isset($_SESSION['panier'])) {
                $_SESSION['panier'] = array();
            }
        }

        array_push($_SESSION['panier'], $productId);

        $number = count($_SESSION['panier']);

        return response()->json(['number' => $number]);
    }

    public function viewBasket() {
        $products = $_SESSION['panier'];
        $response = array();

        if (count($products) > 0)
            $response['success'] = true;
        else
            $response['success'] = false;

        $response['products'] = $products;

        return response()->json($response);
    }

    public function viewAllProducts() {
        return view('home');
    }
}
