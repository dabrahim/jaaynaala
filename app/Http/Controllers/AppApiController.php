<?php

namespace App\Http\Controllers;


use App\Jaaynaala\Components\Model\ApiResponseManager;
use App\Jaaynaala\Components\Model\User;
use App\Jaaynaala\Components\Model\VerificationCode;
use App\Jaaynaala\Components\Service\UserService;
use App\Jaaynaala\Components\Service\VerificationCodeService;
use App\Jaaynaala\Components\Utils;
use Illuminate\Http\Request;

class AppApiController {

    public function loginUser(Request $request) {
        $login = $request->input('login');
        $password = $request->input('password');

        $userService = new UserService();
        if ($userService->isValidAccount($login, $password)) {
            return ApiResponseManager::getSuccessfulResponse("Connexion réussie");
            /*$user = array_filter($userService->findByLogin($login));

            //Account validated
            if (isset($user['validation_date'])) {
                return ApiResponseManager::getSuccessfulResponse("Inscription déroulée avec succès");

            } else {
                $userId = $user['id'];
                $codeService = new VerificationCodeService();
                $fiveDigitsCode = $codeService->getLatestValidCodeByUserId($userId);

                if ($fiveDigitsCode === -1) {
                    $fiveDigitsCode = Utils::generateConfirmationCode();
                    $code = new VerificationCode($fiveDigitsCode, VerificationCode::TYPE_PHONE_VERIFICATION, $userId);
                    $codeService->create($code);
                }

                try {
                    Utils::simulateSendConfirmationCode($fiveDigitsCode);
                    return $fiveDigitsCode;

                } catch (\Exception $e) {
                    return ApiResponseManager::getUnSuccessfulResponse("Une erreur est survenue lors de l'envoi du SMS.");
                }

                //return $fiveDigitsCode;
            }*/

        } else {
            return ApiResponseManager::getUnSuccessfulResponse("Login ou mot de passe incorrect.");
        }
    }

    public function checkCode(Request $request) {
        $code = trim($request->input('confirmationCode'));
        $codeService = new VerificationCodeService();

        if ($codeService->isValid($code)) {
            return ApiResponseManager::getSuccessfulResponse("Le compte a été validé avec succès !");

        } else {
            return ApiResponseManager::getUnSuccessfulResponse("Le code saisi est incorrect ou a déjà expiré.");
        }
    }

    public function registerUser(Request $request) {
        $userPhoneNumber = trim($request->input('phoneNumber'));

        if (strlen($userPhoneNumber) !== 9) {
            return ApiResponseManager::getUnSuccessfulResponse("Le numéro de téléphone doit contenir 9 chiffres. Exemple: 77 419 00 51");
        }

        $userFirstName = $request->input('firstName');
        $userLastName = $request->input('lastName');
        $userPseudo = $request->input('pseudo');
        $userPassword = $request->input('password');

        $user = new User($userPseudo, $userPassword, $userFirstName, $userLastName, User::TYPE_CLIENT, $userPhoneNumber);
        $userService = new UserService();
        $userId = $userService->register($user);

        if ($userId !== -1) {
            //return ApiResponseManager::getSuccessfulResponse("L'inscription s'est déroulée avec succès !");
            $code = new VerificationCode(Utils::generateConfirmationCode(), VerificationCode::TYPE_PHONE_VERIFICATION, $userId);
            $codeService = new VerificationCodeService();

            if ($codeService->create($code)) {
                try {
                    Utils::sendConfirmationCodeSMS($userPhoneNumber, $code->getCode());
                    return ApiResponseManager::getSuccessfulResponse("L'inscription s'est déroulée avec succès !");

                } catch (\Exception $e) {
                    dd($e);
                    return ApiResponseManager::getUnSuccessfulResponse("Une erreur est survenue lors de l'envoi du code de confirmation. Connectez vous avec vos identifiants, nous tenterons de vous renvoyer le code");
                }

            } else {
                return ApiResponseManager::getUnSuccessfulResponse("Une erreur est survenue lors de la génération du code de vérification. Connectez vous avec vos identifiants, nous vous enverrons un nouveau code.");
            }

        } else {
            return ApiResponseManager::getUnSuccessfulResponse("Une erreur inconnue est survenue lors de l'inscription.");
        }
    }

}