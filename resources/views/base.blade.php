<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Jaaynaala, le commerce autrement.</title>
        <link rel="stylesheet" href="{{url('/dist/css/custom/base.min.css')}}">
        <link rel="stylesheet" href="{{url('/dist/css/assets/animate.css')}}">
        @yield('head-includes')
        @yield('styles')
    </head>
    <body>
        <div id="wrapper">@yield('wrapper')</div>
        <div id="layer" class="animated fadeIn">
            <img id="loader" src="{{url('/dist/loaders/dual_ring_white.svg')}}" alt="loader">
            @yield('layer')
        </div>
    </body>
    @yield('foot-includes')
    @yield('scripts')
</html>