@extends('app-base')

@section('styles')
    <link rel="stylesheet" href="{{url(('/dist/css/custom/register.min.css'))}}">
@endsection

@section('content')
    <form action="" method="POST" id="registration-form">
        <img src="{{url('/static/img/jaaynaala.png')}}" alt="logo"><br>
        {{--<h1>Inscription</h1>--}}
        <input required type="text" name="firstName" placeholder="Prénom"><br>
        <input required type="text" name="lastName" placeholder="Nom"><br>
        <input required type="number" name="phoneNumber" placeholder="Numéro de téléphone"><br>
        <input required type="text" name="pseudo" placeholder="Votre pseudo"><br>
        <input required type="password" name="password" placeholder="Votre mot de passe"><br>
        <button type="submit">S'inscrire</button>
    </form>
@endsection

@section('layer')
    <div id="code-verification-block">
        <h1>Code de vérification</h1>
        <p>Veuillez saisir le code de vérification reçu par SMS</p>
        <input type="number" placeholder="Code de vérification" id="confirm-code-input"><br>
        <button id="btn-validate-code">Valider</button>
    </div>
@endsection

@section('scripts')
    <script src="{{url('/dist/js/custom/register.bundle.js')}}" type="application/javascript"></script>
@endsection