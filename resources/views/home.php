<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mon profile</title>
    <link rel="stylesheet" href="static/css/w3.css">
    <link rel="stylesheet" href="static/css/profile.css">
</head>
<body>
    <div class="">
        <div class="w3-bar w3-border">
            <a href="#" class="w3-bar-item">
                <img src="static/img/jaaynaala.png" alt="" style="height : 35px">
            </a>
            <div style="margin-top : 10px">
                <a href="#" class="w3-bar-item">Acceuil</a>
                <div class="w3-dropdown-hover">
                    <button class="w3-button w3-hover-white">Produits</button>
                    <div class="w3-dropdown-content w3-bar-block w3-card-4" style="margin-top : 4px">
                        <a href="mesProduits.php" class="menuProduits">Voir mes produits</a>
                        <a href="ajouterProduit.php" class="menuProduits">Ajouter produits</a>
                    </div>
                </div>
                <a href="#" class="w3-bar-item">Informations de compte</a>
            </div>
            
        </div>
        
        <div class="w3-container w3-center">
                <a href="#" 
                    class="aaddProdBtn w3-button w3-round w3-light-grey w3-hover-text-white w3-hover-orange"
                    onclick="document.getElementById('ajouterProduit').style.display='block'">
                    Ajouter un produit
                </a>
        </div> 

        <div id="ajouterProduit" class="w3-modal">
            <div class="w3-modal-content w3-round">
                <div class="w3-container w3-padding">
                    <span class="w3-right quitter" onclick="document.getElementById('ajouterProduit').style.display='none'">&times;</span>
                    <div>
                        <h3 class="w3-center">Ajouter un produit</h3><hr>
                            <form action="" method="post" class="form">
                                <div class="w3-row">
                                    <div class="w3-col s5">
                                        <input type="text" class="w3-input" name="name" placeholder="Nom du produit...">
                                    </div>
                                    <div class="w3-col s2">
                                        <h3>   </h3>
                                    </div>
                                    <div class="w3-col s5">
                                        <input type="number" class="w3-input" name="price" min="400" placeholder="Prix...">
                                    </div>
                                </div>

                                <div class="w3-row" style="margin-top : 50px">
                                    <div class="w3-col s5">
                                        <textarea type="text" class="w3-input" name="description" placeholder="Description du produit..."></textarea>
                                    </div>
                                    <div class="w3-col s2">
                                        <h3>   </h3>
                                    </div>
                                    <div class="w3-col s5">
                                        <input type="date" class="w3-input" name="publication_date" placeholder="Date de publication...">
                                    </div>
                                </div>
                                <div class="w3-row" style="margin-top : 50px">
                                    <div class="w3-col s12">
                                        <select name="" class="w3-select" placeholder="Sélectionner la">
                                            <option value="">Vêtements</option>
                                            <option value="">Chaussures</option>
                                            <option value="">...</option>
                                            <option value="">...</option>
                                        </select>
                                </div>
                                <div class="w3-center" >
                                    <input type="submit" style="margin-top:20px" class="w3-button w3-orange w3-round w3-text-white" value="Enregister">
                                </div>
                                </div>
                            </form>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</body>
</html>