@extends('app-base')

@section('styles')
    <link rel="stylesheet" href="{{url(('/dist/css/custom/login.min.css'))}}">
@endsection

@section('content')
    <form action="" method="POST" id="login-form">
        <img src="{{url('/static/img/jaaynaala.png')}}" alt="logo"><br>
        <input required type="text" name="login" placeholder="Votre Identifiant"><br>
        <input required type="password" name="password" placeholder="Votre mot de passe"><br>
        <button type="submit">S'inscrire</button>
    </form>
@endsection

@section('scripts')
    <script src="{{url('/dist/js/custom/login.bundle.js')}}" type="application/javascript"></script>
@endsection