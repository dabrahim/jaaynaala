@extends('base')

@section('head-includes')
    <link rel="stylesheet" href="{{url(('/dist/css/custom/main.min.css'))}}">
@endsection

@section('wrapper')
    <nav>
        <img src="{{url('/static/img/jaaynaala.png')}}" class="logo" alt="logo">
        <div class="right">
            <a href="/login">
                <button>Connexion</button>
            </a>
            <a href="/register">
                <button>Inscription</button>
            </a>
        </div>
    </nav>
    <div id="content">
        @yield('content')
    </div>
    <footer></footer>
@endsection