const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: {
        'register': './src/js/register.js',
        'login': './src/js/login.js',
    },
    output: {
        path: path.resolve(__dirname, 'dist/js/custom'),
        filename: '[name].bundle.js'
    },
    module: {
        rules: [{
            test: /\.js?$/,
            exclude: /node_modules|profiles|uploads/,
            loader: 'babel-loader',
            query: {
                presets: ['env']
            }
        }]
    },
    optimization: {
        minimizer: [new UglifyJsPlugin()]
    },
    mode: 'production'
};