import $ from 'jquery'

function blurElement(selector, blur) {
    $(selector).css({
        '-webkit-filter': 'blur(' + blur + 'px)',
        '-moz-filter': 'blur(' + blur + 'px)',
        '-o-filter': 'blur(' + blur + 'px)',
        '-ms-filter': 'blur(' + blur + 'px)',
        'filter': 'blur(' + blur + 'px)'
    });
}

function unBlurElement(selector) {
    $(selector).css({
        '-webkit-filter': 'blur(0)',
        '-moz-filter': 'blur(0)',
        '-o-filter': 'blur(0)',
        '-ms-filter': 'blur(0)',
        'filter': 'blur(0)'
    });
}

let isLoading = false;

function showLoadingLayer() {
    showLayer($('#loader'), 2);
    isLoading = true;
}

function hideElementChildren(element) {
    const alChildren = $(element).children();
    for (const child of alChildren) {
        $(child).hide();
    }
}

function displayAndPositionElementChild(parent, child, weight) {
    const alHeight = $(parent).height();
    const cHeight = $(child).height();
    $(child).css({'marginTop': (alHeight - cHeight) / weight});
    $(child).show();
}

function changeLayerVisibleChild(content, w = 3) {
    const layerElement = $('#layer');
    hideElementChildren(layerElement);
    $(content).show();
    // displayAndPositionElementChild(layerElement, content, w);
    isLoading = false;
}

function showLayer(child) {
    const layerDiv = $('#layer');
    hideElementChildren(layerDiv);
    blurElement("#wrapper", 5);
    $(layerDiv).css('display', 'flex');
    $(child).show();
}

function hideLayer() {
    const actionLayer = $('#layer');
    $(actionLayer).slideUp();
    unBlurElement("#wrapper");
    hideElementChildren(actionLayer);
    isLoading = false;
}

const layer_div = $('#layer');

function disableCloseOnOutsideClick() {
    isLoading = true;
}

function enableCloseOnOutsideClick() {
    isLoading = false;
}

/**
 * Hide the layer when we click outside
 */
$(layer_div).on('click', e => {
    if (!isLoading) {
        let outsideClick = true;

        $(layer_div).children().map((index, child) => {
            if (e.target === child || child.contains(e.target)) {
                outsideClick = false;
            }
        });

        if (outsideClick) {
            hideLayer();
        }
    }
});

export {
    showLayer,
    hideLayer,
    changeLayerVisibleChild,
    showLoadingLayer,
    disableCloseOnOutsideClick
};