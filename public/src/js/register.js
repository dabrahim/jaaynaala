import $ from 'jquery';
import {changeLayerVisibleChild, disableCloseOnOutsideClick, hideLayer, showLayer, showLoadingLayer} from "./base";

$(() => {
    const register_form = $('#registration-form');
    const verificationCode_div = $('#code-verification-block');

    $(register_form).on('submit', e => {
        e.preventDefault();

        const formData = new FormData(e.currentTarget);
        $.ajax({
            url: '/api/users',
            method: 'POST',
            data: formData,
            beforeSend: showLoadingLayer,
            success(result) {
                if (result.success) {
                    changeLayerVisibleChild(verificationCode_div);
                    disableCloseOnOutsideClick();

                } else {
                    alert(result.error.message);
                }
            },
            error() {
                hideLayer();
            },
            processData: false,
            contentType: false
        });
    });

    $('#btn-validate-code').on('click', function () {
        const code = $('#confirm-code-input').val().trim();

        $.post('/api/code/check', {confirmationCode: code}, function (result) {
            if (result.success) {
                window.location = '/login';

            } else {
                alert(result.error.message);
            }
        }, 'json');
    });

});
