import $ from 'jquery';
import {hideLayer, showLoadingLayer} from "./base";

$(() => {
    const login_form = $('#login-form');

    $(login_form).on('submit', e => {
        e.preventDefault();

        const formData = new FormData(e.currentTarget);
        $.ajax({
            url: '/api/users/login',
            method: 'POST',
            data: formData,
            complete: hideLayer,
            beforeSend: showLoadingLayer,
            success(result) {
                if (result.success) {

                } else {
                    alert(result.error.message);
                }
            },
            error() {
            },
            processData: false,
            contentType: false
        });
    });

});